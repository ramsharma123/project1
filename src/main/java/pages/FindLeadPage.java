package pages;

import org.openqa.selenium.WebElement;
//import org.openqa.selenium.interactions.ClickAction;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import okhttp3.internal.cache.DiskLruCache.Editor;
import wdMethods.ProjectMethods;

public class FindLeadPage extends ProjectMethods{

	
	public FindLeadPage() {
		PageFactory.initElements(driver, this);
	}

	//@FindBy(how = How.XPATH, using = "//input[@name='id']")
	
	@FindBy(xpath="//input[@name=\"id\"]") WebElement eleFind;

	public FindLeadPage typeFindId() {		
		type(eleFind, text);
		return this;
	}
	
	@FindBy(xpath="//button[text()='Find Leads']") WebElement eleFindButton;	
	      
	
	public FindLeadPage clickFindLeadButton() throws InterruptedException {		
		click(eleFindButton);
		Thread.sleep(2000);
		return this;
	}


	/*public void clickfindlead() {
		// TODO Auto-generated method stub
		
	}*/
	
	@FindBy(xpath="(//a[@class=\"linktext\"])[4]") WebElement eleleadId;
	
	public Viewlead clickleadId() {
		
		return new Viewlead();
		
	}
	
	/*@FindBy(xpath="//a[text()='Edit']") WebElement eleeditlead;
	public EditLead clickeditlead() {
		
		click(eleeditlead);
		
		return new Editlead() ;
		
	}
	*/

	







}
