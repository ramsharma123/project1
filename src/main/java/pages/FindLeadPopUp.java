
package pages;

import org.openqa.selenium.WebElement;
//import org.openqa.selenium.interactions.ClickAction;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class FindLeadPopUp extends ProjectMethods{

	public FindLeadPopUp() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath="//img[@alt='Lookup']") WebElement eleFromIcok;
	public MergeLeadPopup clickFromIcon() {
		click(eleFromIcok);
		switchToWindow(1);
		return new MergeLeadPopup();
	}
	
	@FindBy(xpath="(//img[@alt='Lookup'])[2]") WebElement eleToIcok;
	public MergeLeadPopup clickToIcon() {
		click(eleFromIcok);
		switchToWindow(1);
		return new MergeLeadPopup();
	}
	
	
	@FindBy(xpath="//a[text()='Merge']") WebElement eleMERGE;
	public Mylead clickMerge() {
		clickWithNoSnap(eleMERGE);
		acceptAlert();
		return new Mylead();
	}
	

	







}
