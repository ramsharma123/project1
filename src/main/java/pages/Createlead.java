package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class Createlead extends ProjectMethods{

  public Createlead() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id="createLeadForm_firstName") WebElement elefname;
	
	@FindBy(id="createLeadForm_lastName") WebElement elelname;
	
	@FindBy(id="createLeadForm_companyName") WebElement elecmyname;
	
	@FindBy(className="smallSubmit") WebElement elecreatelead;
	
	public Createlead elefname(String data) {
		
		type(elefname, data);
		
		return this;
	}
	
	public Createlead elelname(String data) {
		
		type(elelname, data);
		
		return this;
		
	}
	
	public Createlead elecmyname(String data) {
		
		type(elecmyname, data);
		
		return this;
		
  }
	
	public Viewlead clickcreatelead() {
		
		click(elecreatelead);
		
		return new Viewlead();
		
		
	}
	
	
	
	
	
}
