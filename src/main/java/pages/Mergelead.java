package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;


public class Mergelead extends ProjectMethods {
	
	public Mergelead() {
		
		PageFactory.initElements(driver, this);
	}
	
	
	@FindBy(linkText="Merge Leads") WebElement elemergelead;
	
	/*public FindLeadPopUp  Mergelead() {
		click(elemergelead);
		return new FindLeadPopUp();
	}
	
	*/	
	
	public Mergelead elemergelead() {
		click(elemergelead);
         return this;
}
	
	@FindBy (xpath="(//img[@alt='Lookup'])[1]") WebElement eleFromlead;
	
	public FindLeadPopUp Fromlead() {
		click(eleFromlead);
		return new FindLeadPopUp();
		
	}

	

	
	
	

	
	
}
