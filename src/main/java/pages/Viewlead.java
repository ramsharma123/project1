package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class Viewlead extends ProjectMethods{

	
	public Viewlead() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id="viewLead_firstName_sp") WebElement eleverfiyfname;
	
	public Mergelead  verify(String data) {
		
		verifyExactText(eleverfiyfname, data);
		
     	return new Mergelead();  
	  
	  
	}
	
	
	
	
}
