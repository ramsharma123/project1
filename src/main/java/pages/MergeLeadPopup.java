package pages;

import org.openqa.selenium.WebElement;
//import org.openqa.selenium.interactions.ClickAction;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MergeLeadPopup extends ProjectMethods{

	public MergeLeadPopup() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH, using = "//input[@name='firstName']") WebElement eleFindLead;
	public MergeLeadPopup typeFirstName(String data) {
		type(eleFindLead, data);
		return this;
	}

	
	@FindBy(xpath="//button[text()='Find Leads']") WebElement eleFindButton;
	public MergeLeadPopup clickFindButton() throws InterruptedException {
		click(eleFindButton);
		Thread.sleep(2000);
		return this;
	}
	
	@FindBy(xpath="(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]")
	WebElement eleFirstResult;
	public FindLeadPopUp clickFirstResultId() {
		text = eleFirstResult.getText();
		clickWithNoSnap(eleFirstResult);
		switchToWindow(0);
		return new FindLeadPopUp();
	}
	

	







}
