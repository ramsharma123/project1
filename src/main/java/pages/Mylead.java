package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class Mylead extends ProjectMethods{

	public Mylead() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(linkText="Create Lead") WebElement elecreatelead;

	@FindBy(linkText="Find Leads") WebElement elefindlead;

	@FindBy(linkText="Merge Leads") WebElement elemergelead;


	public Createlead clickcreatelead () {

		click(elecreatelead);

		return new Createlead();

	}

	public FindLeadPage clickfindlead () {

		click(elefindlead);
		return new FindLeadPage();

	}

	public FindLeadPopUp clickmergelead () {
		click(elemergelead);
		return new FindLeadPopUp();
	}









}
