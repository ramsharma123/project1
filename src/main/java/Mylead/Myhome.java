package Mylead;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class Myhome extends ProjectMethods{

	public Myhome() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(id = "username") WebElement eleuserName;
	@FindBy(id = "password") WebElement elePassword;
	@FindBy(how = How.CLASS_NAME, using = "decorativeSubmit") WebElement eleLogin;
	@FindBy(how = How.XPATH, using = "//div[@id='errorDiv']/p[2]") WebElement eleErrMsg;
	public Myhome enterUserName(String data) {
		//WebElement eleuserName = locateElement("id", "username");
		type(eleuserName, data);
		return this;
	}
	public Myhome enterPassword(String data) {
		type(elePassword, data);
		return this;
	}
	public HomePage clickLogin() {		
		click(eleLogin);
		//HomePage hp = new HomePage();
		return new HomePage();
	}
	public Myhome clickLoginForFailure() {		
		click(eleLogin);
		//HomePage hp = new HomePage();
		return this;
	}
	
	public Myhome verifyErrorMsg(String data) {		
		verifyPartialText(eleErrMsg, data);
		return this;
	}
	
	
	
	
	
	
}
