package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC003_Mergelead extends ProjectMethods{
	@BeforeTest
	public void setData() {
		testCaseName = "TC003_Mergelead";
		testDescription = "Mergelead";
		authors = "sarath";
		category = "smoke";
		dataSheetName = "TC003";
		testNodes = "Leads";
	}
	
	@Test(dataProvider = "fetchData")
	public void login(String userName,String password, String fromname, String toname) throws InterruptedException {		
		new LoginPage()
		.enterUserName(userName)
		.enterPassword(password)
		.clickLogin()
		.clickcrmsfa()
		.clcickLeads()
		.clickmergelead()
		.clickFromIcon()
		.typeFirstName(fromname)
		.clickFindButton()
		.clickFirstResultId()
		.clickToIcon()
		.typeFirstName(toname)
		.clickFindButton()
		.clickFirstResultId()
		.clickMerge()
		.clickfindlead()
		.typeFindId()
		.clickFindLeadButton();
	
	}
	
}
