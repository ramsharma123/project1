package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC002_Createlead extends ProjectMethods{
	@BeforeTest
	public void setData() {
		testCaseName = "TC002_LoginAndLogout";
		testDescription = "LoginAndLogout";
		authors = "sarath";
		category = "smoke";
		dataSheetName = "TC002";
		testNodes = "Leads";
	}
	
	@Test(dataProvider = "fetchData")
	public void login(String userName,String password, String cname, String fname, String lname) {		
		new LoginPage()
		.enterUserName(userName)
		.enterPassword(password)
		.clickLogin()
		.clickcrmsfa()
		.clcickLeads()
		.clickcreatelead()
		.elecmyname(cname)
		.elefname(fname)
		.elelname(lname)
		.clickcreatelead()
		.verify(fname);
		
		
		
		
		
		/*LoginPage lp = new LoginPage();
		lp.enterUserName();
		lp.enterPassword();
		lp.clickLogin();*/
	}

	
	
}
