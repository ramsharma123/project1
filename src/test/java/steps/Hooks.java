package steps;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import wdMethods.SeMethods;

public class Hooks extends SeMethods{
	
	@Before
	public void before(Scenario sc) {
		System.out.println(sc.getName());
		System.out.println(sc.getId()); 
		startResult();
		startTestModule("CreateLead", "Create a new lead");
		startTestCase("Leads");
		test.assignCategory("Smoke");
		test.assignAuthor("Tl"); 
		startApp("chrome", "http://leaftaps.com/opentaps");	
		
	}
	
	@After
	public void after(Scenario sc) {
		closeAllBrowsers();
		endResult();
		System.out.println(sc.getStatus());  
	}
}
