package steps;

import java.util.concurrent.TimeUnit;

import org.apache.xmlbeans.impl.xb.xsdschema.Public;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;

public class Createlead{

	public ChromeDriver driver;
	@Given("open the browser")

	public void openbrowser() {

		//./driver/chromedriver

		System.setProperty("webdriver.chrome.driver", "E:\\TestLeaf\\PatternObjectModel\\drivers\\chromedriver.exe");
		driver=new ChromeDriver();
	}

	@And("Max the Browser")
	public void maxBrowser() {
		driver.manage().window().maximize();
	}

	@And("Set the Timeout")
	public void timeout() {
		driver.manage().timeouts().implicitlyWait(2000, TimeUnit.SECONDS);
	}

	@And("Lanuch The URL")
	public void lanuchURL() {
		driver.get("http://leaftaps.com/opentaps/control/main");
	}

	@And ("Enter the Username as (.*)")
	public void username(String data) {
		driver.findElementById("username").sendKeys(data);
	}

	@And("Enter the password as (.*)")
	public void password(String data) {
		driver.findElementById("password").sendKeys(data);
	}

	@And("Click on the Login Button")
	public void click() {
		driver.findElementByClassName("decorativeSubmit").click();
	}

	@And("Verify the Login")
	public void verifylogin() {
		System.out.println("verify sucessfully");
	}

	@And("click crmsfa")
	public void clickcrmsfa() {
		driver.findElementByLinkText("CRM/SFA").click();
	}



	@And("Click LeadsButton")
	public void lead() {
		driver.findElementByLinkText("Leads").click();
	}

	@And("Click Createlead")
	public void createlead() {
		driver.findElementByLinkText("Create Lead").click();
	}

	@And("Enter company name as (.*)")
	public void companyname(String data) {
		driver.findElementById("createLeadForm_companyName").sendKeys(data);
	}

	@And("Enter firstname as (.*)")
	public void firstname(String data) {
		driver.findElementById("createLeadForm_firstName").sendKeys(data);
	}

	@And("Enter last name as (.*)")
	public void lastname(String data) {
		driver.findElementById("createLeadForm_lastName").sendKeys(data);
	}

	@And("click creadlead")
	public void  clickcreatelead() {
		driver.findElementByClassName("smallSubmit").click();
	}




}


























